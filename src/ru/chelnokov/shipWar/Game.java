package ru.chelnokov.shipWar;

import java.util.Scanner;

public class Game {


    private static final String INCORRECT_COORDINATE = "Введите число больше, чем 0, но меньше, чем 12";
    private static final String WELCOME = "Добро пожаловать! В канале из 11 клеточек расположен корабль))).";
    private static final String RULES = "Если разрушите его, то будете молодцы-молодцы. Начали!";
    private static final java.lang.String RULES_1 = "Итак, объясняю правила. Вы должны ввести число от 1 до 12 (12 не вводим)";
    private static Scanner reader = new Scanner(System.in);


    public static void main(String[] args) {
        Ship ship = new Ship((int) (Math.random() * 9));
        System.out.println(WELCOME);
        System.out.println(RULES);
        System.out.println(RULES_1);
        shoting(ship);
    }

    /**
     * Метод, проверяющий корректность ввода координаты выстрела
     */

    private static void shoting(Ship ship) {
        int count = 0;
        boolean result = false;
        int attack;
        while (!result) {
            do {
                attack = reader.nextInt();
                if (attack < 1 || attack > 12) {
                    System.out.println(INCORRECT_COORDINATE);
                }
            } while (attack < 1 || attack > 12);
            result = ship.destroyTheShip(attack);
            count++;
            System.out.println("Количество попыток замочить корабль: " + count);
        }
    }

}
