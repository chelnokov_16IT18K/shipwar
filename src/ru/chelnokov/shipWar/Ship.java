package ru.chelnokov.shipWar;


import java.util.ArrayList;

public class Ship {
    private static final String WIN = "Поздравляю, теперь вы молодцы-молодцы!!1";
    private static final String ATTACKED = "Вы попали, но это ещё не всё, молодой человек";
    private static final String NOT_GOT = "Вы промахнулись. Докажите нам, что это была случайность";


    private ArrayList<Integer> location = new ArrayList();

    /**
     * Конструктор, задающий координаты корабля
     */
    Ship(int location) {
        this.location.add(location);
        this.location.add(location + 1);
        this.location.add(location + 2);
    }

    /**
     * Конструктор по умолчанию
     */
    public Ship() {
        this(7);
    }


    public String toString() {
        return "";
    }

    /**
     * Метод, проверяющий, попал ли пользователь в корабль
     *
     * @param attack координата выстрела
     * @return значение true или false
     */

    boolean destroyTheShip(int attack) {
        boolean a;

        if (location.contains(attack)) {
            location.remove(location.indexOf(attack));
            System.out.println(ATTACKED);
            a = false;
        } else {
            System.out.println(NOT_GOT);
            a = false;
        }
        if (location.isEmpty()) {
            System.out.println(WIN);
            a = true;
        }
        return a;
    }
}
